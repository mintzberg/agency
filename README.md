## Agency TestJob - v1.0

### Sources and Credits

PSD: https://freebiesbug.com/psd-freebies/agency-psd-web-template/

####Fonts:
- Google Fonts - https://fonts.google.com/
- Icons Font-face - http://fontawesome.io/
 
####Scripts:
- jQuery - http://jquery.com/
- Bootstrap Framework - https://getbootstrap.com/docs/3.3/
- Owl Carousel 2 - https://owlcarousel2.github.io/OwlCarousel2/
- prettyPhoto - http://www.no-margin-for-errors.com/


