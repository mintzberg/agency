(function ($) {

    "use strict";

    /**
     * Cache jQuery Selector
     */

    var $window = $(window);


    /** Table of jQuery settings list

     00. Preloader
     01. Replace all SVG
     02. Navigation
     03. prettyPhoto
     04. Main slider
     05. Testimonial carousel

     07. When document is Scrolling
     08. When document is loaded
     09. When window is resize

     */


    // 00. Preloader

    function handlePreloader() {
        setTimeout(function () {
            $("#loading").fadeOut(300);
        }, 300);
    }


    // 01. Replace all SVG images with inline SVG

    jQuery('img.svg').each(function () {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
            if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });


    // 02. Navigation

    $(".navbar-nav a").on('click', function (event) {
        if ($("body").hasClass("index")) {
            if (this.hash !== "") {
                event.preventDefault();
                var hash = this.hash;
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function () {
                    window.location.hash = hash;
                });
            }
        }
    });

    $(document).on("click", ".navbar-nav a", function () {
        $(".navbar-nav").find("li").removeClass("active");
        $(this).closest("li").addClass("active");
    });

    $('body').scrollspy({target: '#navigation'});


    // 03. prettyPhoto

    if ($().prettyPhoto) {
        $("a[data-gal^='prettyPhoto']").prettyPhoto({
            theme: 'dark_square'
        });
    }


    // 04. Main slider

    if ($().owlCarousel) {
        $('.main-slider').owlCarousel({
            rtl: false,
            loop: true,
            margin: 10,
            nav: true,
            autoplay: true,
            items: 1,
            navText: [
                '<img src="assets/img/arrow-left.svg" alt="">',
                '<img src="assets/img/arrow-right.svg" alt="">',
            ]
        });
    }


    // 05. Testimonial carousel

    if ($().owlCarousel) {
        $('.testimonial-carousel').owlCarousel({
            rtl: false,
            loop: true,
            margin: 10,
            nav: true,
            autoplay: true,
            responsive: {
                0: {items: 1},
                380: {items: 1},
                668: {items: 1},
                768: {items: 1},
                1200: {items: 1}
            },
            navText: [
                '<img src="assets/img/arrow-left.svg" alt="">',
                '<img src="assets/img/arrow-right.svg" alt="">',
            ]
        });
    }


    // 07. When document is loaded

    $window.on('load', function () {
        (function ($) {
            handlePreloader();
        })(jQuery);
    });


    // 08. When document is Scrolling

    $window.on('scroll', function () {
        (function ($) {
            //
        })(jQuery);
    });


    // 09. When window is resize

    $window.on('resize', function () {
        (function ($) {
            //
        })(jQuery);
    });


}(jQuery));